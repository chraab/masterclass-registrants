# IIHE Masterclass Registrant Tools

(Christoph Raab, December 2018)

## Dependencies
- pandas
- matplotlib
- numpy
- Inkscape (for PDF report)

## Files in this project (unter Git version control):

- convert_csv.sh

    Bash script that converts the Indico CSV from Latin-1 to UTF-8,
    and replaces the long bilingual strings from the registration form
    with short/English ones for better readability. This is mostly useful
    for older Indico versions.
    
    Writes: RegistrantList_readable.csv

- process.ipynb

    Jupyter Python 2 notebook that reads the CSV exported from Indico
    into a data structure, writes the registrant list, produces reports
    as both PDF and HTML and uploads the latter to a webspace.
    Global configuration is loaded from a file.
    
    Writes: graphs/*png, report.html, report.pdf.

- process.py

    Python script version of the same notebook, takes configuration file
    as an argument. Run as `python process.py ./config/my.yaml`.
    
    Writes: graphs/*png, report.html, report.pdf.
    
- config/

    Directory containing institution-specific configuration in the form
    of YAML files. `brussels.yaml` is provided as an example, & contains
    comments that will explain how to edit.


- report_blank.svg

    Template for report slide. Gets compiled to report.pdf via Inkscape.

- report_blank.html

    Template for report website. Gets compiled to report.html.
    
- style.css: Stylesheet belonging to report.html.

- graphs/

    Empty directory, place for generated graphs.

## To do:

- Use Selenium or similar to automatically get CSV from Indico. 

- Use OSM's Nominatim service to make a (clustered) scatter map of participant city names.

- Make PDF report optional, allow using alternative tools to produce PDF

- Add feature to parse data from WIPAC masterclass registration instead of Indico.

