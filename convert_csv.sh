#!/bin/bash

if [ $# -eq 0 ]; then
    echo "Error: no input file name provided!"
    echo
    echo "convert.sh: Convert the CSV file exported from Indico into more usable format"
    echo "Usage: ./convert.sh RegistrantList.csv"
    echo "Output: RegistrantList_readable.csv"
    exit 1
fi

# 1. Latin-1 encoding to UTF-8
iconv -f ISO8859-1 -t UTF8 $1 \
| \
# 2. Replace choices with short names
sed -e s/français/FR/ -e s/vlaams/NL/ \
    -e s/"élève - leerling"/"student"/ -e s/"professeur - leerkracht"/"teacher"/ \
    -e s/"The theory of discovery with IceCube data - Théorie de la découverte avec les données d'IceCube - De theorie van de ontdekking met IceCube gegevens"/"Theory"/ \
    -e s/"Let's discover cosmic rays! - À la découverte des rayons cosmiques - Ontdek de kosmische straling"/"Cosmic Rays"/ \
    -e s/"Je suis... - Ik ben..."/"Role"/ \
    -e s/"Activité de l'après-midi - Namiddagactiviteit"/"Activity"/ \
    -e s/"Dietary restrictions, allergies, etc."/"Diet or allergies"/ \
| \
# 3. Write back to original file
tee ${1%.csv}_readable.csv 1> /dev/null # suppress writing to stdout

