#!/usr/bin/python
# coding: utf-8

import yaml, csv, argparse, codecs, os, subprocess

import numpy as np

import numpy.lib.recfunctions as rf

import matplotlib.pyplot as plt
# Plot style
plt.style.use('dark_background')     # White on black
plt.rcParams["font.family"] = "Muli" # Have this font installed, it looks nice
plt.rcParams["font.weight"] = "bold"

from datetime import datetime

import pandas as pd

from glob import glob

from tools import Translator, SafeFormatter

parser = argparse.ArgumentParser(description='Process Masterclass registrant data from Indico.')

parser.add_argument('configfile', default='./config/brussels.yaml', nargs='?',
                    help='Configuration file for institution-specific settings. Make your own copy and edit according to comments in there.')

args = parser.parse_args()

# # Global configuration

with open(args.configfile) as yf:
    config = yaml.load(yf)

# # Load CSV

# ## Read file, converting from Latin-1 to UTF-8

with open(config['csv_path']) as f:
    raw_reg = [tuple([unicode(cell.rstrip().lstrip(), config['csv_encoding']) for cell in row]) for row in csv.reader(f)]

# ## Load into data structure

# Shorten the column names already at this point
shortify_columns = Translator(config['table_columns'])
columns = map(shortify_columns, raw_reg[0])

# Remove byte order mark at start of file
columns[0] = columns[0].lstrip(u'\ufeff')

# use a numpy structured array
reg = np.array(raw_reg[1:], dtype=[(str(col), 'U64') for col in columns])

# sort by date
date_column = [col for col in ['Registration Date','Registration date'] if col in reg.dtype.names][0]
reg = rf.append_fields(reg, 'isodate', [pdate.isoformat() for pdate in pd.to_datetime(reg[date_column], infer_datetime_format=True)])
reg = np.sort(reg, order='isodate')

# ## Shorten strings for choices

shortify_choices = Translator(config['table_choices'])
for col in ['Language', 'Role', 'Activity']:
    reg[col] = map(shortify_choices, reg[col])

# ## Amend missing data (by guessing they are students)

for r_i in reg:
    if not r_i['Role']:
        r_i['Role'] = 'student'

# # Registration date plot

date_status = datetime.fromtimestamp(os.path.getmtime(config['csv_path'])).isoformat()

delta = (pd.to_datetime(date_status) - pd.to_datetime(reg[date_column], infer_datetime_format=True))

delta_days = - (delta.days + delta.seconds/3600./24. + delta.microseconds/1e6/3600./24.)

span = delta_days.max()-delta_days.min()

fig1, ax1 = plt.subplots(figsize=(2*span/17,2*span/17))

ax1.spines["top"].set_visible(False)
ax1.spines["right"].set_visible(False)
ax1.spines["left"].set_visible(False)

h,b,p=ax1.hist(delta_days, bins=np.arange(delta_days.min(), delta_days.max()+2) - 0.5,
               histtype='bar', color='w', lw=1, edgecolor='k')
plt.yticks([])

plt.ylim(0,h.max())

plt.xlabel('days ago')
[plt.axhline(y, color='k',lw=1) for y in np.arange(h.max()+1)]

plt.xlim(delta_days.min()-0.5,0.5)

ax1.set_aspect('equal', 'box') # equal aspect and resize the axes so the data fits

#plt.autoscale()
plt.savefig('graphs/graph.date.png',dpi=300, bbox_inches='tight')

# # Donut charts

def absolute_format(pct, allvals):
    absolute = int(pct/100.*np.sum(allvals)+0.01)
    return "{:d}".format(absolute)

def donut_chart(reg, column):
    labels= np.unique(reg[column])#[reg['Role']=='student'])
    size_d = {l:((reg[column]==l)).sum() for l in  labels}
    sizes = np.array([size_d[l] for l in  labels])
    is_other = sizes < sizes.sum()/12.
    other_sum = sizes[is_other].sum()
    if other_sum>0:
        sizes = np.append(sizes[~is_other], other_sum)
        labels = np.append(labels[~is_other], 'other')
    
    fig, ax = plt.subplots(figsize=(2,2))

    wedges, texts, autotexts =     ax.pie(sizes, #labels=labels,
            wedgeprops=dict(width=0.6, linewidth=2, edgecolor='k'),
            colors=['w' for s in sizes],
            autopct=lambda pct: absolute_format(pct, sizes),
            startangle=89.9,
            labeldistance=0.8,
            pctdistance=0.7,
            )
    plt.setp(autotexts,color='k', fontsize=14)

    ax.set_aspect('equal', 'box')  # Equal aspect ratio ensures that pie is drawn as a circle.

    bbox_props = dict(boxstyle="square,pad=0.1", fc="none", ec="none", lw=0.72)
    kw = dict(xycoords='data', textcoords='data', arrowprops=dict(arrowstyle="-"),
              bbox=bbox_props, zorder=0, va="center", fontsize=12)

    for i, p in enumerate(wedges):
        ang = (p.theta2 - p.theta1)/2. + p.theta1
        y = np.sin(np.deg2rad(ang))
        x = np.cos(np.deg2rad(ang))
        horizontalalignment = {-1: "right", 1: "left"}[int(np.sign(x))]
        connectionstyle = "angle,angleA=0,angleB={}".format(ang)
        kw["arrowprops"].update({"connectionstyle": connectionstyle, 'lw':2})
        ax.annotate(labels[i], xy=(x, y), xytext=(1.15*np.sign(x), 1.2*y),#xytext=(1.35*np.sign(x), 1.4*y),
                     horizontalalignment=horizontalalignment, **kw)

    plt.text(0.5,0.5, str(sum(sizes)),
             horizontalalignment='center',
             verticalalignment='center',
             transform=ax.transAxes,
             fontdict=dict(color='w', fontsize=14))

    plt.savefig('graphs/graph.{0}.png'.format(column.lower()), bbox_inches='tight')

donut_chart(reg, 'Language')

donut_chart(reg, 'Activity')

donut_chart(reg, 'Institution')

# # Write name list

with codecs.open(config['list_file'], 'w', encoding='utf8') as f:
    for row in reg:#[reg['Role']=='student']:
        f.write(row['Surname']+','+row['First Name']+'\n')

# # Make reports

dates = reg['isodate']
dates = [d.split('T')[0] for d in dates]

data = dict(   n_FR=((reg['Role']=='student')*(reg['Language']=='FR')).sum(),
               n_NL=((reg['Role']=='student')*(reg['Language']=='NL')).sum(),
               n_student = (reg['Role']=='student').sum(),
               n_teacher=(reg['Role']=='teacher').sum(),
               d_most_recent = dates[-1],
               d_median = dates[len(dates)/2],
               d_downloaded = date_status[:date_status.rfind(':')].replace('T',' '),
           )

# ## PDF Slide

def produce_report(template='report_blank.svg', outdir='./', filename='report.pdf', tempfile='_specif.svg', **data):
    
    with open(template) as f:
        template = f.read() # load template
    
    filename = os.path.join(outdir, filename)
    
    specif = SafeFormatter().vformat(template, (), data) # fill in template, skip unmatched since there are curly braces in the SVG files output by matplotlib
    
    with open(tempfile,'w') as f:
        f.write(specif) # write to temporary file
    
    subprocess.call("inkscape --export-pdf={fn} {temp}".format(fn=filename, temp=tempfile).split()) # convert to PDF
    
    os.remove(tempfile)


produce_report(**data)

# ## Webpage

def produce_website(template='report_blank.html', outdir='./', filename='report.html', **data):
    
    with open(template) as f:
        template = f.read() # load template
    
    filename = os.path.join(outdir, filename)
    
    specif = SafeFormatter().vformat(template, (), data) # fill in template, skip unmatched since there are curly braces in the CSS
    
    with open(filename,'w') as f:
        f.write(specif) 
        
    for file_to_transfer in [filename, 'style.css', 'graphs']:
        subprocess.call("rsync -avz {ft} {ssh_host}:{ssh_path}".format(ft=file_to_transfer, **config).split()) # upload to webspace
    
    
produce_website(**data)
    

