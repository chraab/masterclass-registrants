from collections import defaultdict
import string

class SafeDict( dict ):
    def __missing__(self, key):
        return '{' + key + '}'
    
class Translator( object ):
    def __init__(self, table):
        self.table = table
    def forward(self, string):
        for k,v in self.table:
            string = string.replace(k,v)
        return string
    def backward(self, string):
        for k,v in self.table:
            string = string.replace(v,k)
        return string
    def __call__(self, string):
        return self.forward(string)
    
class SafeFormatter(string.Formatter):
    def __init__(self, *args, **kwargs):
        self._special_escaper = Translator([(':','__colon__'), (';','__semicolon__'), ('-','__dash__')])
        return super(SafeFormatter, self).__init__(*args, **kwargs)
    def vformat(self, format_string, args, kwargs):
        escaped = self._special_escaper.forward(format_string)
        formatted = super(SafeFormatter, self).vformat(escaped, args, SafeDict(**kwargs))
        descaped = self._special_escaper.backward(formatted)
        return descaped
